"use strict";

function currentTime(totalDuration) {
	var startTime = 1499703318;
	var currentTime = Math.round(new Date().getTime() / 1000);
	var difference = currentTime - startTime;
	var ratio = difference / totalDuration;
	var ratioFloor = Math.floor(ratio);
	var currentRatio = ratio - ratioFloor;
	var currentVideo = totalDuration * currentRatio;
	return currentVideo;
}

function YTDurationToSeconds(duration) {
	var match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/);

	var hours = parseInt(match[1]) || 0;
	var minutes = parseInt(match[2]) || 0;
	var seconds = parseInt(match[3]) || 0;

	return hours * 3600 + minutes * 60 + seconds;
}

function exportJSON(data) {
	//Get the file contents
	var txtFile = "test.txt";
	var file = new File([""], txtFile);
	var str = JSON.stringify(data);

	//Save the file contents as a DataURI
	var dataUri = 'data:application/json;charset=utf-8,' + encodeURIComponent(str);

	//Write it as the href for the link
	var link = document.getElementById('link').href = dataUri;
}

function setRepetition(callback, delay, repetition) {
	var i = 0;
	var interval = setInterval(function () {
		i++;
		callback(i);
		if (i === repetition) {
			clearInterval(interval);
		}
	}, delay);
}

var vis = function () {
	var stateKey,
	    eventKey,
	    keys = {
		hidden: "visibilitychange",
		webkitHidden: "webkitvisibilitychange",
		mozHidden: "mozvisibilitychange",
		msHidden: "msvisibilitychange"
	};
	for (stateKey in keys) {
		if (stateKey in document) {
			eventKey = keys[stateKey];
			break;
		}
	}
	return function (c) {
		if (c) document.addEventListener(eventKey, c);
		return !document[stateKey];
	};
}();

function multiTouch(target, callback) {
	interact(target).on('tap', function (event) {
		callback(event);
	});
}
'use strict';

//Default
$(function () {
	$('.content--video').hover(function () {
		$('.content--toolbar').removeClass('hide');
	}, function () {
		$('.content--toolbar').addClass('hide');
	});
});

eventKeydown(true);
$('.l--form input').focus(function () {
	eventKeydown(false);
});
$('.l--form input').focusout(function () {
	eventKeydown(true);
});
$("body").tooltip({
	selector: '[data-toggle="tooltip"]'
});

if ($(window).width() < 480 || $(window).height() < 480) {
	$('.js-play-btn').removeClass('hide');
	$('.js-logout').parent().addClass('hide');
	$('.toolbar-desktop').addClass('hide');
	$('.toolbar-mobile').removeClass('hide');
}

//LA Time
setInterval(function () {
	var utcTime = new Date();
	var laOffset = -7 * 60 * 60000;
	var userOffset = utcTime.getTimezoneOffset() * 60000;
	var laTime = new Date(utcTime.getTime() + laOffset + userOffset);
	var hours = laTime.getHours();
	var minutes = laTime.getMinutes();
	var mid = 'AM';
	if (minutes < 10) {
		minutes = '0' + minutes.toString();
	}
	if (hours === 0) {
		hours = 12;
	} else if (hours > 12) {
		hours = hours % 12;
		mid = 'PM';
	}
	$('.js-la-time').text(hours + ':' + minutes + ' ' + mid);
}, 1000);

//Factory
var clickTrigger = function clickTrigger(trigger, target) {
	multiTouch(trigger, function () {
		$('.l--view').addClass('hide');
		$(target).parent().removeClass('hide');
	});
};

var clickListen = function clickListen(trigger, callback) {
	multiTouch(trigger, callback);
};

function toggleView(payload) {
	var element = payload.element;
	var option = payload.option;
	if (option === undefined) {
		if ($(element).hasClass('hide')) {
			$(element).removeClass('hide');
		} else {
			$(element).addClass('hide');
		}
	}
	if (option === 'hide') {
		$(element).addClass('hide');
	}
	if (option === 'view') {
		$(element).removeClass('hide');
	}
}

function toggleFullScreen() {
	if (document.fullScreenElement && document.fullScreenElement !== null || !document.mozFullScreen && !document.webkitIsFullScreen) {
		if (document.documentElement.requestFullScreen) {
			document.documentElement.requestFullScreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullScreen) {
			document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}

		$('.content').addClass('col-md-12');
		$('.content').removeClass('col-md-9');
		$('.chat').addClass('hide');
	} else {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}

		$('.content').removeClass('col-md-12');
		$('.content').addClass('col-md-9');
		$('.chat').removeClass('hide');
	}
}

function eventKeydown(state) {
	if (state === true) {
		$(document).keydown(function () {
			$('.js-m').focus();
		});
	}
	if (state === false) {
		$(document).off('keydown');
	}
}

//Execute Factory
clickTrigger('.js-btn-register', '.js-register');
clickTrigger('.js-btn-login', '.js-login');
clickListen('.js-fullscreen', toggleFullScreen);
clickListen('.js-sound', toggleSound);
clickListen('.js-emoji-btn', function () {
	toggleView({ element: '.js-emoji-panel' });
});
clickListen('.js-play-btn', function () {
	$('.content--play-btn').addClass('hide');
	resumeVideo();
});
clickListen('.js-show-user', function () {
	if ($('.chat--online').hasClass('hide')) {
		$('.chat--menu').addClass('hide');
		$('.chat--online').removeClass('hide');
	} else {
		$('.chat--menu').addClass('hide');
		$('.chat--messages').removeClass('hide');
	}
});
clickListen('.js-profile', function () {
	if ($('.chat--profile').hasClass('hide')) {
		$('.chat--menu').addClass('hide');
		$('.js-chat').addClass('hide');
		$('.chat--profile').removeClass('hide');
	} else {
		$('.chat--menu').addClass('hide');
		$('.js-chat').removeClass('hide');
		$('.chat--messages').removeClass('hide');
	}
});
clickListen('.js-giveaway', function () {
	if ($('.chat--giveaway').hasClass('hide')) {
		$('.chat--menu').addClass('hide');
		$('.js-chat').addClass('hide');
		$('.chat--giveaway').removeClass('hide');
	} else {
		$('.chat--menu').addClass('hide');
		$('.js-chat').removeClass('hide');
		$('.chat--messages').removeClass('hide');
	}
});
clickListen('.js-scoreboard', function () {
	if ($('.chat--scoreboard').hasClass('hide')) {
		$('.chat--menu').addClass('hide');
		$('.js-chat').addClass('hide');
		$('.chat--scoreboard').removeClass('hide');
	} else {
		$('.chat--menu').addClass('hide');
		$('.js-chat').removeClass('hide');
		$('.chat--messages').removeClass('hide');
	}
});
clickListen('.js-chat-toggle', function () {
	if ($('.content').hasClass('col-md-12')) {
		$('.content').removeClass('col-md-12');
		$('.content').addClass('col-md-9');
		$('.chat').removeClass('hide');
	} else {
		$('.content').addClass('col-md-12');
		$('.content').removeClass('col-md-9');
		$('.chat').addClass('hide');
	}
});

function chatBottom() {
	var cb = document.getElementsByClassName("js-messages")[0];
	if (cb.scrollHeight - cb.scrollTop - cb.clientHeight <= 100) {
		cb.scrollTop = cb.scrollHeight;
	}
}
'use strict';

$(function () {
	var packet = {};
	var store = {
		isLoggedIn: false,
		emoji: [],
		notif: 0,
		initiated: false,
		streamState: 'streaming',
		user: {
			username: ''
		}

		//Account state
	};var accountState = function accountState() {
		if (store.isLoggedIn === true) {
			$('.v-hide').addClass('hide');
			$('.v-loggedIn').removeClass('hide');
		}
		if (store.isLoggedIn === false) {
			$('.v-hide').addClass('hide');
			$('.v-loggedOut').removeClass('hide');
		}
	};

	//Emoji
	var emojiInitiate = function emojiInitiate() {
		multiTouch('.js-emoji.emoji--click', function (e) {
			var em = $(e.target).data("emoji");
			var m = $('.js-m').val();
			m = m + ' :' + em + ' ';
			$('.js-m').val(m);
			$('.js-m').focus();
		});
	};

	var emoji = function emoji(_emoji, emojiStore) {
		interact('.js-emoji.emoji--click').off('tap');
		$('.js-emoji-store').empty();
		emojiStore.forEach(function (x) {
			var val = x.val;
			var reason = x.reason;
			var src = 'emoji/' + val + '.png';
			var img = $('<img>').attr({
				'src': src,
				'data-toggle': 'tooltip',
				'data-emoji': val
			}).addClass('js-emoji emoji--thumb');
			if (_emoji.filter(function (x) {
				return x === val;
			}).length === 1) {
				img.addClass('emoji--click').attr({
					'title': reason
				});
			}
			$('.js-emoji-store').append(img);
		});
		emojiInitiate();
	};

	//Functions

	var getQueryVariable = function getQueryVariable(variable) {
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split("=");
			if (pair[0] === variable) {
				return pair[1];
			}
		}
		return false;
	};

	var setCookie = function setCookie(name, value) {
		var days = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 7;
		var path = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '/';

		var expires = new Date(Date.now() + days * 864e5).toGMTString();
		document.cookie = name + '=' + encodeURIComponent(value) + '; expires=' + expires + '; path=' + path;
	};

	var getCookie = function getCookie(name) {
		return document.cookie.split('; ').reduce(function (r, v) {
			var parts = v.split('=');
			return parts[0] === name ? decodeURIComponent(parts[1]) : r;
		}, '');
	};

	var deleteCookie = function deleteCookie(name, path) {
		setCookie(name, '', -1, path);
	};

	//Listeners

	var socket = io().connect();
	socket.on('connect', function () {
		var token = getCookie('token');
		if (token !== 'undefined') {
			socket.emit('authenticate', token);
		}
		console.log('connected');
		socket.emit('connected');
	});

	socket.on('initiate', function (packet) {
		console.log('initiated');
		syncVideo(packet.videoStore, 'initiate');
	});

	socket.on('premiere', function (packet) {
		premiereVideo(packet.premiereStart, packet.videoStore, packet.option);
		//syncVideo(packet.videoStore, packet.option)
		setRepetition(function (i) {
			var li = $('<li>');
			var ntf = 'l--notification';
			var sec = 4 - i;
			var msg = '';
			if (sec !== 0) {
				msg = '<span>Premiere in ' + sec + '</span>';
			} else {
				msg = '<span>Premiere Now!</span>';
			}
			li.addClass(ntf).html(msg);
			$('.js-messages').append(li);
		}, 1000, 4);
		$('.js-premiere-offline').addClass('hide');
		$('.js-premiere-online').removeClass('hide');
	});

	socket.on('authenticate', function (packet) {
		setCookie('token', packet.token);
		store.isLoggedIn = packet.isLoggedIn;
		store.emoji = packet.emoji;
		store.user.username = packet.username;
		emoji(store.emoji, packet.emojiStore);
		accountState();
		$('.js-m').focus();
		$('.js-points-chat').text(packet.chat);
		$('.js-points-vlog').text(packet.vlog);
		$('.js-points-total').text(packet.total);
		//Giveaway
		$('.js-val-instagram').text(packet.instagram);
		$('.js-val-twitter').text(packet.twitter);
		$('.js-val-facebook').text(packet.facebook);
		$('.js-val-email').text(packet.email);
		$('.js-val-country').text(packet.country);
	});

	socket.on('scoreboard', function (packet) {
		packet.forEach(function (x) {
			var li = $('<li>');
			var text = x.username + ' | ' + x.points;
			var spr = $('<span>').text(text);
			if (x.decorator !== undefined) {
				spr.addClass('l--' + x.decorator);
			}
			li.append(spr);
			$('.js-scores').append(li);
		});
	});

	socket.on('emoji', function (packet) {
		store.emoji = packet.emoji;
		emoji(store.emoji, packet.emojiStore);
	});

	socket.on('error', function (packet) {
		if (packet.purpose === 'login') {
			$('.js-error-login').removeClass('hide');
		}
	});

	socket.on('refreshState', function (packet) {
		setCookie('token', packet.token);
		store.isLoggedIn = packet.isLoggedIn;
		store.emoji = packet.emoji;
		emoji(store.emoji);
		accountState();
		$('.js-m').focus();
	});

	socket.on('logout', function (packet) {
		deleteCookie('token');
		store.isLoggedIn = false;
		accountState();
	});

	socket.on('settings', function (packet) {
		if (packet.code === 200) {
			$('.js-settings-notif').removeClass('hide');
			$('.js-settings-notif').text('You\'ve changed your name to ' + packet.username);
		}
		if (packet.code === 400) {
			$('.js-settings-notif').removeClass('hide');
			$('.js-settings-notif').text('Username is taken');
		}
	});

	socket.on('chat', function (packet) {
		var li = $('<li>');

		if (packet.usr !== undefined) {
			var usr = $('<span>').addClass('l--usr').html(packet.usr);
			var spr = $('<span>').text(': ');
			if (packet.decorator !== undefined) {
				usr.addClass('l--' + packet.decorator);
			}
			var msg = $('<span>').addClass('l--msg').html(packet.msg);
			li.append(usr).append(spr).append(msg);

			if (packet.usr === store.user.username) {
				//li.addClass('l--mention')
			}
		}

		if (packet.ntf !== undefined) {
			li.addClass(packet.ntf).html(packet.msg);
		}

		$('.js-messages').append(li);
		$('.js-m').focus();
		chatBottom();

		if (!vis()) {
			store.notif = store.notif + 1;
			document.title = '(' + store.notif + ') Logangster - Logan Paul Vlogs Live Streaming';
		}
	});

	socket.on('connectChat', function (packet) {
		if (store.initiated === false) {
			store.initiated = true;
			packet.chatHistory.reverse().forEach(function (x) {
				var li = $('<li>');
				var usr = $('<span>').addClass('l--usr').html(x.username);
				if (x.decorator !== undefined) {
					usr.addClass('l--' + x.decorator);
				}
				var spr = $('<span>').text(': ');
				var msg = $('<span>').addClass('l--msg').html(x.message);
				li.append(usr).append(spr).append(msg);
				$('.js-messages').append(li);
				$('.js-m').focus();
				chatBottom();
			});
		}
	});

	socket.on('connectMeta', function (packet) {
		if (packet.userOnline !== undefined) {
			$('.js-user-online').empty();
			packet.userOnline.forEach(function (x) {
				var li = $('<li>');
				var text = x.username + ' | ' + x.points;
				var spr = $('<span>').text(text);
				if (x.decorator !== undefined) {
					spr.addClass('l--' + x.decorator);
				}
				li.append(spr);
				$('.js-user-online').append(li);
			});
		}
		if (packet.countConnect !== undefined) {
			$('.js-count').text(packet.countConnect);
		}
	});

	socket.on('startVideo', function (packet) {
		loadVideo(packet, 0);
	});

	//Triggers

	$('.js-chat').submit(function () {
		packet.msg = $('.js-m').val();
		if (store.isLoggedIn === true && packet.msg.length !== 0) {
			if (packet.msg.length >= 2) {
				socket.emit('chat', packet);
			}
			if (packet.msg.length < 2) {
				socket.emit('chat-ntf');
			}
		}
		toggleView({
			element: '.js-emoji-panel',
			option: 'hide'
		});
		$('.js-m').val('');
		return false;
	});

	$('.js-login').submit(function () {
		packet.username = $('.js-login .js-username').val();
		packet.password = $('.js-login .js-password').val();
		socket.emit('login', packet);
		return false;
	});

	$('.js-register').submit(function () {
		packet.username = $('.js-register .js-username').val();
		packet.password = $('.js-register .js-password').val();
		socket.emit('register', packet);
		return false;
	});

	$('.js-settings').click(function () {
		packet.username = $('.js-settings .js-username').val();
		socket.emit('settings', packet);
		return false;
	});

	$('.js-form-giveaway').click(function () {
		packet.instagram = $('.js-form-giveaway .js-val-instagram').val();
		packet.twitter = $('.js-form-giveaway .js-val-twitter').val();
		packet.facebook = $('.js-form-giveaway .js-val-facebook').val();
		packet.email = $('.js-form-giveaway .js-val-email').val();
		packet.country = $('.js-form-giveaway .js-val-country').val();
		socket.emit('giveaway', packet);
		return false;
	});

	$('.js-logout').click(function () {
		socket.emit('logout');
		interact('.js-emoji.emoji--click').off('tap');
	});

	var buffer = 0;
	$(document).on('vlogEnded', function () {
		if (store.isLoggedIn === true) {
			if (buffer === 0) {
				buffer = 1;
				console.log('Vlog Ended!');
				socket.emit('vlog');
			}
			setTimeout(function () {
				buffer = 0;
			}, 3000);
		}
	});

	//Default

	accountState();
	vis(function () {
		if (vis()) {
			store.notif = 0;
		}
		document.title = 'Logangster - Logan Paul Vlogs Live Streaming';
	});
});
'use strict';

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var store = {
	videoStore: [],
	videoTotalDuration: '',
	currentVideoIndex: '',
	playbackStatus: ''
};
var premiereStore = {
	premiereStart: '',
	premiereState: 'offline'
};
var player = void 0;

function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		playerVars: {
			'autoplay': 1,
			'controls': 0,
			'rel': 0,
			'modestbranding': 1,
			'showinfo': 1,
			'disablekb': 1,
			'enablejsapi': 1,
			'fs': 0,
			'playsinline': 1,
			'autohide': 2,
			'widgetid': 1,
			'cc_load_policy': 0
		},
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}

function onPlayerReady(event) {
	var storeOption = void 0;

	if (premiereStore.premiereState === 'online') {
		storeOption = 'premiere';
	} else {
		storeOption = undefined;
	}

	if (store.playbackStatus === 'dataReady') {
		syncVideo(store, storeOption);
	} else {
		store.playbackStatus = 'playerReady';
	}
}

function resumeVideo() {
	if (premiereStore.premiereState === 'online') {
		syncVideo(store, 'premiere');
	} else {
		syncVideo(store);
	}
}

function premiereVideo(data, dataVideo) {
	var option = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : premiere;

	console.log(dataVideo);
	console.log('premiere option', option);
	premiereStore.premiereStart = data;
	premiereStore.premiereState = 'online';

	syncVideo(dataVideo, option);
}

function syncVideo(data) {
	var option = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

	var storeData = data;
	var currentDuration = currentTime(storeData.videoTotalDuration);
	var currentVideoIndex = storeData.videoStore.findIndex(function (x) {
		if (currentDuration < x.videoSeconds) {
			return true;
		} else {
			currentDuration -= x.videoSeconds;
		}
	});

	var temporaryStore = {
		video: storeData.videoStore[currentVideoIndex],
		videoStore: storeData.videoStore,
		currentDuration: currentDuration,
		currentVideoIndex: currentVideoIndex,
		videoTotalDuration: storeData.videoTotalDuration

		// if(premiereStore.premiereState === 'online'){
		// 	option = 'premiere'
		// }

	};if (option === 'premiere' || option === 'initiatePremiere') {
		temporaryStore.video = storeData.videoStore[storeData.videoStore.length - 1];
		temporaryStore.currentDuration = Date.now() / 1000 - premiereStore.premiereStart;
		//temporaryStore.videoTotalDuration = storeData.videoStore.length
	}

	startYouTube(temporaryStore, option);
}

function startYouTube(packet) {
	var option = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;


	var video = packet.video;
	var payload = {
		videoId: video.videoId,
		videoPublishedAt: video.videoPublishedAt,
		startSeconds: packet.currentDuration,
		videoTitle: video.videoTitle,
		currentVideoIndex: packet.currentVideoIndex + 1,
		totalVideo: packet.videoStore.length
	};

	if (option === undefined) {
		loadVideo(payload);
	}
	if (option === 'premiere') {
		loadVideo(payload);
	}
	if (option === 'initiate' || option === 'initiatePremiere') {
		if (store.playbackStatus === 'playerReady') {
			loadVideo(payload);
		} else {
			store.playbackStatus = 'dataReady';
		}
	}

	if (option !== 'premiere') {
		store.currentVideoIndex = packet.currentVideoIndex + 1;
	}
	store.videoStore = packet.videoStore;
	store.videoTotalDuration = packet.videoTotalDuration;
}

function loadVideo(packet) {

	var videoId = packet.videoId;
	var startSeconds = packet.startSeconds;
	var videoTitle = packet.videoTitle;
	var currentVideoIndex = packet.currentVideoIndex;
	var totalVideo = packet.totalVideo;
	var date = new Date(packet.videoPublishedAt);
	var videoPublishedAt = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

	player.loadVideoById({
		'videoId': videoId,
		'startSeconds': startSeconds,
		//'endSeconds': startSeconds+5,
		'suggestedQuality': 'large'
	});

	$('.js-video-title').text(videoTitle);
	$('.js-video-current').text(currentVideoIndex);
	$('.js-video-total').text(totalVideo);
	$('.js-video-published-at').text(videoPublishedAt);
}

function onPlayerStateChange(event) {
	if (event.data === 0) {
		if (premiereStore.premiereState === 'online') {
			premiereStore.premiereState = 'offline';
			$('.js-premiere-offline').removeClass('hide');
			$('.js-premiere-online').addClass('hide');
		}
		syncVideo(store);
		$(document).trigger('vlogEnded');
	}
	if (event.data === 1) {
		$('.content--play-btn').addClass('hide');
	}
	if (event.data === 2) {
		$('.content--play-btn').removeClass('hide');
	}
}

function stopVideo() {
	player.stopVideo();
}

function toggleSound() {
	if (player.isMuted()) {
		player.unMute();
	} else {
		player.mute();
	}
}

//# sourceMappingURL=script.js.map