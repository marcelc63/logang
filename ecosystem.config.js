module.exports = {
  apps: [{
    name: 'logang',
    script: './index.js'
  }],
  deploy: {
    production: {
      user: 'ec2-user',
      host: 'ec2-52-221-48-209.ap-southeast-1.compute.amazonaws.com',
      key: '/Users/marcelchristianis/Project/Personal/Logang/logang.pem',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:marcelc63/logang.git',
      path: '/home/ec2-user/app',
      'post-deploy': 'npm install && pm2 startOrRestart ecosystem.config.js'
    }
  }
}
