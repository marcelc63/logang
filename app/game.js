//Dependencies
var parser = require('../app/parser.js');
var query = require('../app/query.js');

function chat(x,store,emojiStore,io,socket){
  if(x.data.chat === 10 && x.meta.code === 200){
    query.emoji({emoji:4,identifier:store.identifier}, (x)=>{
      store.emoji.push('thasssmuhboiii')
      let payload = {
        msg: '<div><p>Congratulations! You\'ve unlocked :thasssmuhboiii from sending 10 messages!</p></div>',
        ntf: 'l--register',
        purpose: 'msg'
      }
      payload.msg = parser.emoji(payload.msg,store.emoji)
      io.to(socket.id).emit('emoji', {
        emoji:store.emoji,
        emojiStore: emojiStore
      })
      io.to(socket.id).emit('chat', payload);
    })
  }

  if(x.data.chat === 50 && x.meta.code === 200){
    query.emoji({emoji:5,identifier:store.identifier}, (x)=>{
      store.emoji.push('kong')
      let payload = {
        msg: '<div><p>Congratulations! You\'ve unlocked :kong from sending 50 messages!</p></div>',
        ntf: 'l--register',
        purpose: 'msg'
      }
      payload.msg = parser.emoji(payload.msg,store.emoji)
      io.to(socket.id).emit('emoji', {
        emoji:store.emoji,
        emojiStore: emojiStore
      })
      io.to(socket.id).emit('chat', payload);
    })
  }
}

function gamification(){
  function chatUnlock(option,callback){
    if(x.data.chat === option.emoji.amount && x.meta.code === 200){
      query.emoji({emoji:option.emoji.id,identifier:option.store.identifier}, (x)=>{
        store.emoji.push(option.emoji.name)
        let payload = {
          msg: option.emoji.message,
          ntf: 'l--register',
          purpose: 'msg'
        }
        payload.msg = parser.emoji(payload.msg,store.emoji)
        io.to(socket.id).emit('emoji', {
          emoji:option.store.emoji,
          emojiStore: option.store.emojiStore
        })
        io.to(socket.id).emit('chat', payload);
      })
    }
  }

  chatUnlock({
    emoji: {
      id: 5,
      name: 'kong',
      amount: 50,
      message: '<div><p>Congratulations! You\'ve unlocked :kong from sending 50 messages!</p></div>'
    },
    store: {
      emoji: store.emoji,
      emojiStore: emojiStore,
      identifier: store.identifier,
    }
  })
}

module.exports = {
  chat: chat
}

//Backup Codes

if(x.data.chat === 10 && x.meta.code === 200){
  query.emoji({emoji:4,identifier:store.identifier}, (x)=>{
    store.emoji.push('thasssmuhboiii')
    let payload = {
      msg: '<div><p>Congratulations! You\'ve unlocked :thasssmuhboiii from sending 10 messages!</p></div>',
      ntf: 'l--register',
      purpose: 'msg'
    }
    payload.msg = parser.emoji(payload.msg,store.emoji)
    io.to(socket.id).emit('emoji', {
      emoji:store.emoji,
      emojiStore: emojiStore
    })
    io.to(socket.id).emit('chat', payload);
  })
}

if(x.data.chat === 50 && x.meta.code === 200){
  query.emoji({emoji:5,identifier:store.identifier}, (x)=>{
    store.emoji.push('kong')
    let payload = {
      msg: '<div><p>Congratulations! You\'ve unlocked :kong from sending 50 messages!</p></div>',
      ntf: 'l--register',
      purpose: 'msg'
    }
    payload.msg = parser.emoji(payload.msg,store.emoji)
    io.to(socket.id).emit('emoji', {
      emoji:store.emoji,
      emojiStore: emojiStore
    })
    io.to(socket.id).emit('chat', payload);
  })
}
