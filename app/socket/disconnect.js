module.exports = function (io, socket, main) {    
    let store = socket.store
    
    socket.on('disconnect', function (packet) {
        main.countConnect = main.countConnect - 1
        io.emit('countConnect', main.countConnect);
        if (store.isLoggedIn === true) {
            main.userOnline = main.userOnline.filter(x => x.username !== store.username)
            let connectMeta = {
                userOnline: main.userOnline
            }
            io.emit('connectMeta', connectMeta)
        }
    });
}