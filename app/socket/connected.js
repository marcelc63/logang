var parser = require('../../app/parser.js')
var query = require('../../app/query.js')

module.exports = function (io, socket, main) {    
        let store = socket.store

        socket.on('connected', function (packet) {            
            console.log('connected')
            let emojiDefault = main.emojiStore.reduce((a, b) => {
                return a.concat(b.val);
            }, [])

            query.chat('', (x) => {
                let chatHistory = x.data.chat.map(x => {
                    return {
                        username: x.username,
                        decorator: x.decorator,
                        message: parser.emoji(x.message, emojiDefault),
                        messageDv: x.message
                    }
                })
                if (main.streamState === 'streaming') {
                    let initiatePacket = {
                        videoStore: main.videoStore
                    }
                    io.to(socket.id).emit('initiate', initiatePacket);
                } else {
                    console.log('connected premiere')
                    let premierePacket = {
                        videoStore: main.videoStore,
                        premiereStart: main.premiereStore.premiereStart,
                        option: 'initiatePremiere'
                    }
                    io.to(socket.id).emit('premiere', premierePacket);
                }

                let connectMeta = {
                    countConnect: main.countConnect,
                    userOnline: main.userOnline
                }
                let connectChat = {
                    chatHistory: chatHistory
                }
                io.emit('connectMeta', connectMeta)
                io.to(socket.id).emit('connectChat', connectChat)
            }, 'history')

            query.stats('', (x) => {
                io.to(socket.id).emit('scoreboard', x.data)
            }, 'scoreboard')
        })    
}