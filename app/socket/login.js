var query = require('../../app/query.js')

module.exports = function (io, socket, main) {
    let socketAuthenticate = require('./functions/socketAuthenticate.js')(io, socket, main).execute

    socket.on('login', function (packet) {
        query.login({
            username: packet.username,
            password: packet.password
        }, (x) => {
            if (x.meta.code === 200) {
                let payload = {
                    msg: '<span>Welcome back!</span>',
                    msgDv: 'Welcome back!',
                    ntf: 'l--notification',
                    purpose: 'msg'
                }
                socketAuthenticate(x, payload)
            }
            if (x.meta.code === 400) {
                io.to(socket.id).emit('error', {
                    purpose: 'login'
                });
            }
        })
    })
}