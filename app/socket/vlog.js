var query = require('../../app/query.js')

module.exports = function (io, socket, main) {
    let store = socket.store
    let gamification = require('./functions/gamification.js')(io, socket, main).execute

    socket.on('vlog', function (packet) {
        if (store.isLoggedIn === true) {
            query.stats({
                identifier: store.identifier
            }, (x) => {
                query.stats({
                    identifier: store.identifier
                }, (x) => {
                    gamification({
                        amount: x.data.vlog,
                        code: x.meta.code
                    }, 'vlog')
                }, 'check')
            }, 'update_vlog')
        }
    })
}