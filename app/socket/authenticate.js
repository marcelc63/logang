var query = require('../../app/query.js')

module.exports = function (io, socket, main) {
    let socketAuthenticate = require('./functions/socketAuthenticate.js')(io, socket, main).execute

    socket.on('authenticate', function (packet) {
        query.authenticate({
            token: packet
        }, (x) => {
            socketAuthenticate(x)
        })
    })
}