var query = require('../../app/query.js')

module.exports = function (io, socket, main) {
    let store = socket.store

    socket.on('settings', function (packet) {
        if (packet.username.length !== 0) {
            query.settings({
                username: packet.username,
                identifier: store.identifier
            }, (x) => {
                if (x.meta.code === 200) {
                    main.userOnline = main.userOnline.filter(x => x.username !== store.username)
                    store.username = x.data.username
                    connectMeta(true)
                    io.to(socket.id).emit('settings', {
                        code: 200,
                        username: store.username
                    })
                }
                if (x.meta.code === 400) {
                    io.to(socket.id).emit('settings', {
                        code: 400
                    })
                }
            })
        }
    })
}