var parser = require('../../app/parser.js')
var query = require('../../app/query.js')

module.exports = function (io, socket, main) {
    let store = socket.store
    let socketAuthenticate = require('./functions/socketAuthenticate.js')(io, socket, main).execute

    socket.on('register', function (packet) {
        query.register({
            username: packet.username,
            password: packet.password
        }, (x) => {
            if (x.meta.code === 200) {
                query.login({
                    username: packet.username,
                    password: packet.password
                }, (x) => {
                    let payload = {
                        msg: '<div><p>Welcome to the place where all Logangsters gather and watch together!</p> <p>As a new member, you\'ve unlocked the following emoji: :logang :ugudbro.</p> <p>You can unlock more emoji by being active!</p> <p>Have fun :)</p></div>',
                        msgDv: 'Welcome to the place where all Logangsters gather and watch together! As a new member, you\'ve unlocked the following emoji: :logang :ugudbro. You can unlock more emoji by being active! Have fun!',
                        ntf: 'l--register',
                        purpose: 'msg'
                    }
                    payload.msg = parser.emoji(payload.msg, store.emoji)
                    socketAuthenticate(x, payload)
                })
            }
        })
    })
}