module.exports = function (io, socket, main) {
    socket.on('startVideo', function (packet) {
        io.emit('startVideo', packet);
    });
}