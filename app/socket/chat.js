var parser = require('../../app/parser.js')
var query = require('../../app/query.js')

module.exports = function (io, socket, main) {    
    let store = socket.store
    let gamification = require('./functions/gamification.js')(io, socket, main).execute
    let moderateUnmute = require('./functions/moderateUnmute.js')(io, socket, main).execute
    

    socket.on('chat', function (packet) {
        if (store.isLoggedIn === true && store.isMute === false && packet.msg.length < 2) {
            let payload = {
                msg: '<span>Are you trying to spam? Please send a longer and meaningful message. Let\'s make this place fun!</span>',
                msgDv: 'Are you trying to spam? Please send a longer and meaningful message. Let\'s make this place fun!',
                ntf: 'l--notification',
                purpose: 'msg'
            }
            io.to(socket.id).emit('chat', payload);
        } else if (store.isLoggedIn === true && store.isMute === false && packet.msg.length >= 2) {
            //Spam Prevention
            if (store.muteState === true) {
                store.muteCount = store.muteCount + 1
            }
            if (store.muteCount === 5) {
                store.isMute = true
                query.moderating({
                    mute: 1,
                    identifier: store.identifier
                }, () => { })
                moderateUnmute()
            }
            if (store.muteState === false) {
                setTimeout(() => {
                    store.muteState = false
                    store.muteCount = 0
                }, 5000)
            }
            store.muteState = true

            let payload = {
                msg: packet.msg,
                msgDv: packet.msg,
                usr: store.username,
                decorator: store.decorator,
                purpose: 'chat'
            }
            payload.msg = parser.sanitize(payload.msg)
            payload.msg = parser.emoji(payload.msg, store.emoji)
            io.emit('chat', payload)

            query.chat({
                identifier: store.identifier,
                username: store.username,
                message: packet.msg,
                decorator: store.decorator,
                timestring: new Date().getTime()
            })

            query.stats({
                identifier: store.identifier
            }, (x) => {
                query.stats({
                    identifier: store.identifier
                }, (x) => {
                    gamification({
                        amount: x.data.chat,
                        code: x.meta.code
                    }, 'chat')
                }, 'check')
            }, 'update_chat')
        } else if (store.isLoggedIn === true && store.isMute === true && packet.msg.length !== 0) {
            let payload = {
                msg: '<span>You\'ve been muted due to excessive spamming. You\'ll be unmuted in five minutes.</span>',
                msgDv: 'You\'ve been muted due to excessive spamming. You\'ll be unmuted in five minutes.</span>',
                ntf: 'l--notification',
                purpose: 'msg'
            }
            io.to(socket.id).emit('chat', payload);
        } else {
            let payload = {
                msg: '<span>Login or Register to Join Chat</span>',
                msgDv: 'Login or Register to Join Chat',
                ntf: 'l--notification',
                purpose: 'msg'
            }
            io.to(socket.id).emit('chat', payload);
        }
    });

    socket.on('chat-ntf', function (packet) {
        let payload = {
            msg: '<span>Are you trying to spam? Please send a longer and meaningful message. Let\'s make this place fun!</span>',
            msgDv: 'Are you trying to spam? Please send a longer and meaningful message. Let\'s make this place fun!',
            ntf: 'l--notification',
            purpose: 'msg'
        }
        io.to(socket.id).emit('chat', payload);
    }) 
}