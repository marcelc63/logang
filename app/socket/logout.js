module.exports = function (io, socket, main) {
    let store = socket.store
    
    socket.on('logout', function (packet) {
        store.isLoggedIn = false
        main.userOnline = main.userOnline.filter(x => x.username !== store.username)
        let connectMeta = {
            userOnline: main.userOnline
        }
        io.emit('connectMeta', connectMeta)
        let payload = {
            msg: '<span>You\'ve logged out</span>',
            msgDv: 'You\'ve logged out',
            ntf: 'l--notification',
            purpose: 'msg'
        }
        io.to(socket.id).emit('chat', payload);
        io.to(socket.id).emit('logout');
    });
}