module.exports = function (io, socket, main) {
    let connectMeta = require('./connectMeta.js')(io, socket, main).execute
    let moderateUnmute = require('./moderateUnmute.js')(io, socket, main).execute
    let store = socket.store

    const socketAuthenticate = (x, payload = undefined) => {
        if (x.meta.code === 200) {

            //Control Mute
            if (x.data.mute === 1) {
                store.isMute = true
                moderateUnmute()
            }

            store.username = x.data.username
            store.token = x.data.token
            store.identifier = x.data.identifier
            store.decorator = x.data.decorator
            store.emoji = new Set([...store.emoji, ...x.data.emoji])
            store.emoji = [...store.emoji]
            store.isLoggedIn = true
            store.chat = x.data.chat
            store.vlog = x.data.vlog
            //Giveaway Data
            store.instagram = x.data.instagram
            store.twitter = x.data.twitter
            store.facebook = x.data.facebook
            store.email = x.data.email
            store.country = x.data.country

            connectMeta()
            let points = store.chat + (store.vlog * 2)

            io.to(socket.id).emit('authenticate', {
                isLoggedIn: store.isLoggedIn,
                token: store.token,
                emoji: store.emoji,
                emojiStore: main.emojiStore,
                username: store.username,
                chat: store.chat,
                vlog: store.vlog,
                total: points,
                instagram: store.instagram,
                twitter: store.twitter,
                facebook: store.facebook,
                email: store.email,
                country: store.country
            })

            if (payload !== undefined && payload.purpose === 'msg') {
                io.to(socket.id).emit('chat', payload);
            }

            //Join Broadcast
            let joining = {
                msg: '<span><b>' + store.username + ' | ' + points + ' points</b> is online</span>',
                msgDv: '' + store.username + ' | ' + points + ' points is online',
                ntf: 'l--notification',
                purpose: 'msg'
            }
            io.emit('chat', joining);

            //Special Emoji Giveaway
            // gamification({
            //   amount: 0,
            //   code: 200
            // }, 'special')

        }
        if (x.meta.code === 400) {
            store.isLoggedIn = false
            io.to(socket.id).emit('logout');
        }
    }

    return {
        execute: socketAuthenticate
    }
}