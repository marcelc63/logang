var parser = require('../../../app/parser.js')
var query = require('../../../app/query.js')

module.exports = function (io, socket, main) {
    let store = socket.store

    //Gamification
    function gamification(res, option = undefined) {
        function unlock(option, callback) {
            if (option.res.amount >= option.emoji.amount && option.res.code === 200) {
                query.emoji({
                    emoji: option.emoji.id,
                    identifier: option.store.identifier
                }, (x) => {
                    store.emoji.push(option.emoji.name)
                    let payload = {
                        msg: option.emoji.message,
                        msgDv: option.emoji.messageDv,
                        ntf: 'l--register',
                        purpose: 'msg'
                    }
                    payload.msg = parser.emoji(payload.msg, store.emoji)
                    io.to(socket.id).emit('emoji', {
                        emoji: option.store.emoji,
                        emojiStore: option.store.emojiStore
                    })
                    io.to(socket.id).emit('chat', payload);
                })
            }
        }

        if (option === 'special') {
            unlock({
                emoji: {
                    id: 25,
                    name: 'outtamyhair',
                    amount: 0,
                    message: '<div><p>Happy Logan Day! To celebrate Logan Paul Day we\'re giving you :outtamyhair</p></div>',
                    messageDv: 'Happy Logan Day! To celebrate Logan Paul Day we\'re giving you :outtamyhair'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 26,
                    name: 'outtamyhairlogan',
                    amount: 0,
                    message: '<div><p>Happy Logan Day! To celebrate Logan Paul Day we\'re giving you :outtamyhairlogan</p></div>',
                    messageDv: 'Happy Logan Day! To celebrate Logan Paul Day we\'re giving you :outtamyhairlogan'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })
        }

        if (option === 'chat') {
            unlock({
                emoji: {
                    id: 4,
                    name: 'thasssmuhboiii',
                    amount: 1,
                    message: '<div><p>You\'ve sent your first message! Congratulations! You\'ve unlocked :thasssmuhboiii</p></div>',
                    messageDv: 'You\'ve sent your first message! Congratulations! You\'ve unlocked :thasssmuhboiii'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 5,
                    name: 'kong',
                    amount: 10,
                    message: '<div><p>Congratulations! You\'ve unlocked :kong from sending 10 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :kong from sending 10 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 13,
                    name: 'ayla',
                    amount: 20,
                    message: '<div><p>Congratulations! You\'ve unlocked :ayla from sending 20 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :ayla from sending 20 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 15,
                    name: 'smash',
                    amount: 40,
                    message: '<div><p>Congratulations! You\'ve unlocked :smash from sending 40 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :smash from sending 40 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 17,
                    name: 'yahyeet',
                    amount: 70,
                    message: '<div><p>Congratulations! You\'ve unlocked :yahyeet from sending 70 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :yahyeet from sending 70 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 18,
                    name: 'ayoooo',
                    amount: 100,
                    message: '<div><p>Congratulations! You\'ve unlocked :ayoooo from sending 100 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :ayoooo from sending 100 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 19,
                    name: 'brendan',
                    amount: 200,
                    message: '<div><p>Congratulations! You\'ve unlocked :brendan from sending 200 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :brendan from sending 200 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 21,
                    name: 'lydia',
                    amount: 300,
                    message: '<div><p>Congratulations! You\'ve unlocked :lydia from sending 300 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :lydia from sending 300 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 23,
                    name: 'oyla',
                    amount: 400,
                    message: '<div><p>Congratulations! You\'ve unlocked :oyla from sending 400 messages!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :oyla from sending 400 messages!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })
        }
        if (option === 'vlog') {
            unlock({
                emoji: {
                    id: 10,
                    name: 'mark',
                    amount: 1,
                    message: '<div><p>You\'ve watched your first Vlog! Congratulations! You\'ve unlocked :mark</p></div>',
                    messageDv: 'You\'ve watched your first Vlog! Congratulations! You\'ve unlocked :mark'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 11,
                    name: 'george',
                    amount: 3,
                    message: '<div><p>Congratulations! You\'ve unlocked :george from watching 3 vlogs!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :george from watching 3 vlogs!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 12,
                    name: 'evan',
                    amount: 5,
                    message: '<div><p>Congratulations! You\'ve unlocked :evan from watching 5 vlogs!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :evan from watching 5 vlogs!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 14,
                    name: 'savage',
                    amount: 8,
                    message: '<div><p>Congratulations! You\'ve unlocked :savage from watching 8 vlogs!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :savage from watching 8 vlogs!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 16,
                    name: 'whatpoppin',
                    amount: 12,
                    message: '<div><p>Congratulations! You\'ve unlocked :whatpoppin from watching 12 vlogs!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :whatpoppin from watching 12 vlogs!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 20,
                    name: 'johannes',
                    amount: 20,
                    message: '<div><p>Congratulations! You\'ve unlocked :johannes from watching 20 vlogs!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :johannes from watching 20 vlogs!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 22,
                    name: 'maverick',
                    amount: 30,
                    message: '<div><p>Congratulations! You\'ve unlocked :maverick from watching 30 vlogs!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :maverick from watching 30 vlogs!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })

            unlock({
                emoji: {
                    id: 24,
                    name: 'savagee',
                    amount: 40,
                    message: '<div><p>Congratulations! You\'ve unlocked :savagee from watching 40 vlogs!</p></div>',
                    messageDv: 'Congratulations! You\'ve unlocked :savagee from watching 40 vlogs!'
                },
                store: {
                    emoji: store.emoji,
                    emojiStore: main.emojiStore,
                    identifier: store.identifier,
                },
                res: {
                    amount: res.amount,
                    code: res.code
                }
            })
        }
    }

    return {
        execute: gamification
    }
    //Gamification Ends
}