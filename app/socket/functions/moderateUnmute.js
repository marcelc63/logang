var query = require('../../../app/query.js')

module.exports = function (io, socket, main) {
    let store = socket.store
    
    function moderateUnmute() {
        setTimeout(() => {
            store.isMute = false
            store.muteCount = 0
            query.moderating({
                mute: 0,
                identifier: store.identifier
            }, () => { })
            let payload = {
                msg: '<span>You\'ve been unmuted and your point has been deducted. Please don\'t do this again. It\'s not fun when people spam.</span>',
                msgDv: 'You\'ve been unmuted and your point has been deducted. Please don\'t do this again. It\'s not fun when people spam.',
                ntf: 'l--notification',
                purpose: 'msg'
            }
            io.to(socket.id).emit('chat', payload);
        }, 300000)
    }

    return {
        execute: moderateUnmute
    }
}