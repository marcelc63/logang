module.exports = function (io, socket, main) {
    let store = socket.store
    
    const connectMeta = (payload = undefined) => {
        if (!main.userOnline.some(x => x === store.username)) {
            let points = store.chat + (store.vlog * 2)
            main.userOnline = main.userOnline.concat({
                username: store.username,
                points: points,
                decorator: store.decorator
            })
            let connectMeta = {
                userOnline: main.userOnline
            }
            io.emit('connectMeta', connectMeta)

        }
    }

    return {
        execute: connectMeta
    }
}