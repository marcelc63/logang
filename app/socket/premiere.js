var schedule = require('node-schedule');
let video = require('../../app/video.js');
var admin = require("firebase-admin");

// var serviceAccount = require("../config/logangster-e646c-firebase-adminsdk-t3rnf-c216b23edb.json");

// admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount),
//     databaseURL: "https://logangster-e646c.firebaseio.com"
// });

module.exports = function (io, main) {   
    //Schedule
    function premiereEnd(premiereDuration) {
        let endTime = premiereDuration * 1000
        console.log(endTime)
        setTimeout(() => {
            main.streamState = 'streaming'
            console.log('premiere ends')
        }, endTime);
    }
    //* 17-20 * * *
    //*/5 18-23 * * *
    // function schedulePremiere() {
    //     console.log('premiere scheduled')
    //     var j = schedule.scheduleJob('*/5 18-23 * * *', () => {
    //         video.initiate((x) => {
    //             console.log('cron')
    //             let video = x.data.videoStore
    //             let videoNow = main.videoStore.videoStore
    //             if (video[video.length - 1].videoId !== videoNow[videoNow.length - 1].videoId) {
    //                 console.log('premiere')
    //                 main.videoStore = x.data
    //                 main.streamState = 'premiere'
    //                 main.premiereStore.premiereDuration = video[video.length - 1].videoSeconds
    //                 main.premiereStore.premiereStart = Math.round(new Date().getTime() / 1000)
    //                 premiereEnd(main.premiereStore.premiereDuration)
    //                 let premierePacket = {
    //                     videoStore: main.videoStore,
    //                     premiereStart: main.premiereStore.premiereStart,
    //                     option: 'premiere'
    //                 }
    //                 io.sockets.emit('premiere', premierePacket);
    //                 j.cancel()
    //             }
    //         }, 'premiere')
    //     })
    // }
    // schedulePremiere()

    // var k = schedule.scheduleJob('0 23 * * *', () => {
    //     schedulePremiere()
    // })

    function premierePushNotif(){
        // The topic name can be optionally prefixed with "/topics/".
        var topic = "general";

        // See the "Defining the message payload" section below for details
        // on how to define a message payload.
        var payload = {
            notification: {
                title: "What's Up Logang!",
                body: "We're going to watch the latest Logan Paul Vlog in 5 minutes!"
            }
        };

        // Send a message to devices subscribed to the provided topic.
        admin.messaging().sendToTopic(topic, payload)
            .then(function (response) {
                // See the MessagingTopicResponse reference documentation for the
                // contents of response.
                console.log("Successfully sent message:", response);
            })
            .catch(function (error) {
                console.log("Error sending message:", error);
            });
    }

    function premiereVideo(){
        console.log('together')
        video.initiate((x) => {
            console.log('cron together')
            let video = x.data.videoStore
            let videoNow = main.videoStore.videoStore
            console.log('premiere')
            main.videoStore = x.data
            main.streamState = 'premiere'
            main.premiereStore.premiereDuration = video[video.length - 1].videoSeconds
            main.premiereStore.premiereStart = Math.round(new Date().getTime() / 1000)
            premiereEnd(main.premiereStore.premiereDuration)
            let premierePacket = {
                videoStore: main.videoStore,
                premiereStart: main.premiereStore.premiereStart,
                option: 'premiere'
            }
            io.sockets.emit('premiere', premierePacket);
        }, 'premiere')
    }

    //Schedule Push Notif
    schedule.scheduleJob('55 17 * * *', () => {
        premierePushNotif()
    })
    schedule.scheduleJob('55 21 * * *', () => {
        premierePushNotif()
    })

    //Schedule Premiere
    schedule.scheduleJob('0 18 * * *', () => {
        premiereVideo()
    })
    schedule.scheduleJob('0 22 * * *', () => {
        premiereVideo()
    })
}