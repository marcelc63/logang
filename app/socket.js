//Dependencies
var parser = require('../app/parser.js');
var query = require('../app/query.js');
var schedule = require('node-schedule');
var admin = require("firebase-admin");

var serviceAccount = require("./config/logangster-e646c-firebase-adminsdk-t3rnf-c216b23edb.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://logangster-e646c.firebaseio.com"
});

//Print Server Time
console.log("Server Hour", new Date().getHours() + " " + new Date().getMinutes())

//Video
let main = {
  videoStore: '',
  emojiStore: [],
  streamState: 'streaming',
  premiereStore: {
    premiereDuration: '',
    premiereStart: ''
  },
  countConnect: 0,
  userOnline: []
}

let video = require('../app/video.js')
video.initiate((x) => {
  main.videoStore = x.data
  //videoStore.videoStore.splice(-1,1)
  console.log(main.videoStore.videoStore.length)
  console.log('initiated')
})

//Define
query.emoji('', (x) => {
  main.emojiStore = x.data.emoji
}, 'update')
module.exports = function (http) {
  var io = require('socket.io')(http);    

  // middleware
  io.use((socket, next) => {
    if (socket.handshake.headers.referer === 'http://localhost:3000/' || socket.handshake.headers.referer === 'https://logangster.com/'){
      return next();
    }
    else if (socket.handshake.query.token === 'ilovekimchi'){
      return next();
    }          
    else {
      return next(new Error('authentication error'));
    }    
  })

  //Premiere
  require('./socket/premiere.js')(io, main)

  //Connection
  io.on('connection', function (socket) {
    console.log('connection')    
    socket.store = {
      username: '',
      token: '',
      identifier: '',
      decorator: undefined,
      isLoggedIn: false,
      isMute: false,
      muteState: false,
      muteCount: 0,      
      emoji: ['logang', 'ugudbro', 'logan', 'survey', 'yes', 'no', 'circleone']
    }

    //Define
    let store = socket.store
    main.countConnect = main.countConnect + 1;
    if(main.countConnect == 1 || main.countConnect % 5 == 0){
      // The topic name can be optionally prefixed with "/topics/".
      var topic = "general";

      // See the "Defining the message payload" section below for details
      // on how to define a message payload.
      var payload = {
        notification: {
          title: "Logangsters are Online",
          body: "Come and join the fun!"
        }
      };

      // Send a message to devices subscribed to the provided topic.
      admin.messaging().sendToTopic(topic, payload)
        .then(function (response) {
          // See the MessagingTopicResponse reference documentation for the
          // contents of response.
          console.log("Successfully sent message:", response);
        })
        .catch(function (error) {
          console.log("Error sending message:", error);
        });
    }

    //Connection        
    require('./socket/connected.js')(io, socket, main)
    require('./socket/disconnect.js')(io, socket, main)
    require('./socket/settings.js')(io, socket, main)
    require('./socket/giveaway.js')(io, socket, main)
    require('./socket/mobileConnect.js')(io, socket, main)
    
       //Chat
    require('./socket/chat.js')(io, socket, main)

    //Vlog
    require('./socket/vlog.js')(io, socket, main)

    //Account State    
    require('./socket/login.js')(io, socket, main)
    require('./socket/logout.js')(io, socket, main)
    require('./socket/register.js')(io, socket, main)
    require('./socket/authenticate.js')(io, socket, main)
    require('./socket/startVideo.js')(io, socket, main)

  });
}