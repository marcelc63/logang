const bcrypt = require('bcrypt-nodejs');
const mysql = require('mysql');

function devMode(x) {
  if (x === 'server') {
    return mysql.createPool({
      connectionLimit: 1000,
      host: 'logang.ckheswsqzkrq.ap-southeast-1.rds.amazonaws.com',
      user: 'logang',
      password: 'Marcel6363',
      database: 'logang'
    });
  }
  if (x === 'dev') {
    return mysql.createPool({
      connectionLimit: 1000,
      host: 'logang.ckheswsqzkrq.ap-southeast-1.rds.amazonaws.com',
      user: 'logang',
      password: 'Marcel6363',
      database: 'dev'
    });
  }
  if (x === 'localhost') {
    return mysql.createPool({
      connectionLimit: 200,
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'logang'
    });
  }
}
const pool = devMode('server')


const callbackCheck = (callback, data) => {
  if (data === 400) {
    data = {
      meta: {
        code: 400,
      }
    }
  }
  if (callback !== undefined) {
    callback(data)
  }
}

const mysqlQuery = (sql, callback) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(sql, function (err, result, fields) {
      if (err) throw err;
      callback(result, fields)
      connection.release()
    });
  });
}

const emojiConstruct = (payload, option = undefined) => {
  let emoji = []
  for (i in payload) {
    if (option === 'user') {
      emoji.push(payload[i].val)
    }
    if (option === 'store') {
      emoji.push({
        'val': payload[i].val,
        'reason': payload[i].reason
      })
    }
  }
  return emoji
}

const emoji = (payload, callback, option = undefined) => {
  function emojiCheck(payload, callback, option = undefined) {
    mysqlQuery({
      sql: 'SELECT * FROM `emoji` WHERE emoji=? AND identifier=?',
      timeout: 40000, // 40s
      values: [payload.emoji, payload.identifier]
    }, (x) => {
      if (x.length === 0) {
        emojiInsert(payload, callback, option)
      }
    })
  }

  function emojiInsert(payload, callback, option = undefined) {
    mysqlQuery({
      sql: 'INSERT INTO `emoji` (emoji,identifier)VALUE(?,?)',
      timeout: 40000, // 40s
      values: [payload.emoji, payload.identifier]
    }, (x) => {
      if (x.affectedRows === 1) {
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  if (option === undefined) {
    emojiCheck(payload, callback, option)
  }

  if (option === 'authenticate') {    
    mysqlQuery({
      sql: 'SELECT emoji_list.val as val, emoji.identifier as identifier FROM `emoji` LEFT JOIN emoji_list ON emoji.emoji = emoji_list.key WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.identifier]
    }, (x) => {      
      if (x.length !== 0 || x.length === 0) {        
        let data = {
          meta: {
            code: 200,
          },
          data: {
            username: payload.username,
            token: payload.token,
            identifier: payload.identifier,
            decorator: payload.decorator,
            mute: payload.mute,
            emoji: emojiConstruct(x, 'user'),
            chat: payload.chat,
            vlog: payload.vlog,
            instagram: payload.instagram,
            twitter: payload.twitter,
            facebook: payload.facebook,
            email: payload.email,
            country: payload.country            
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  if (option === 'update') {
    mysqlQuery({
      sql: 'SELECT * FROM `emoji_list` ORDER BY `key` ASC',
      timeout: 40000, // 40s
    }, (x) => {
      let data = {
        meta: {
          code: 200,
        },
        data: {
          emoji: emojiConstruct(x, 'store')
        }
      }
      callbackCheck(callback, data)
    })
  }
}

const chat = (payload, callback, option = undefined) => {
  if (option === undefined) {
    mysqlQuery({
      sql: 'INSERT INTO `chat_history` (identifier,username,decorator,message,timestring)VALUE(?,?,?,?,?)',
      timeout: 40000, // 40s
      values: [payload.identifier, payload.username, payload.decorator, payload.message, payload.timestring]
    }, (x) => {
      if (x.affectedRows === 1) {
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }
  if (option === 'history') {
    mysqlQuery({
      sql: 'SELECT * FROM `chat_history` ORDER BY id DESC LIMIT 0,10',
      timeout: 40000 // 40s
    }, (x) => {
      let data = {
        meta: {
          code: 200,
        },
        data: {
          chat: x
        }
      }
      callbackCheck(callback, data)

    })
  }
}

const moderating = (payload, callback, option = undefined) => {
  if (option === undefined || option === 'mute') {
    mysqlQuery({
      sql: 'UPDATE `user` SET `mute` = ? WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.mute, payload.identifier]
    }, (x) => {
      if (x.affectedRows === 1) {
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }  
}

const giveaway = (payload, callback, option = undefined) => {
  if (option === undefined || option === 'update') {
    mysqlQuery({
      sql: 'UPDATE `user` SET `email` = ?, `twitter` = ?, `instagram` = ?, `country` = ?, `facebook` = ? WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.email, payload.twitter, payload.instagram, payload.country, payload.facebook, payload.identifier]
    }, (x) => {
      if (x.affectedRows === 1) {
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  if (option === 'check') {
    mysqlQuery({
      sql: 'SELECT * FROM `user` WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.identifier]
    }, (x) => {
      if (x.length === 1) {
        let data = {
          meta: {
            code: 200,
          },
          data: {
            email: x[0].email,
            twitter: x[0].twitter,
            country: x[0].country,
            facebook: x[0].facebook,
            instagram: x[0].instagram,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }
}

const stats = (payload, callback, option = undefined) => {
  if (option === 'register') {
    mysqlQuery({
      sql: 'INSERT INTO `stats` (identifier,chat,vlog)VALUE(?,?,?)',
      timeout: 40000, // 40s
      values: [payload.identifier, 0, 0]
    }, (x) => {
      if (x.affectedRows === 1) {
        console.log(payload.username + ' registered')
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  if (option === undefined || option === 'update_chat') {
    mysqlQuery({
      sql: 'UPDATE `stats` SET `chat` = `chat` + 1 WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.identifier]
    }, (x) => {
      if (x.affectedRows === 1) {
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  if (option === 'update_vlog') {
    mysqlQuery({
      sql: 'UPDATE `stats` SET `vlog` = `vlog` + 1 WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.identifier]
    }, (x) => {
      if (x.affectedRows === 1) {
        let data = {
          meta: {
            code: 200,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  if (option === 'check') {
    mysqlQuery({
      sql: 'SELECT * FROM `stats` WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.identifier]
    }, (x) => {
      if (x.length === 1) {
        let data = {
          meta: {
            code: 200,
          },
          data: {
            chat: x[0].chat,
            vlog: x[0].vlog
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  if (option === 'scoreboard') {
    let sql = 'SELECT user.username as username, (stats.chat + stats.vlog*2) as points FROM `user` LEFT JOIN `stats` on stats.identifier = user.identifier ORDER BY points DESC LIMIT 0, 20'
    mysqlQuery({
      sql: sql,
      timeout: 40000, // 40s      
    }, (x) => {
      let data = {
        meta: {
          code: 200,
        },
        data: x
      }
      callbackCheck(callback, data)
    })
  }
}

const register = (payload, callback) => {
  payload.password = bcrypt.hashSync(payload.password, bcrypt.genSaltSync(8), null)
  payload.identifier = bcrypt.hashSync(payload.username + new Date().getTime(), bcrypt.genSaltSync(8), null)

  if (payload.username.length !== 0) {
    mysqlQuery({
      sql: 'SELECT * FROM `user` WHERE `username` = ?',
      timeout: 40000, // 40s
      values: [payload.username]
    }, (x) => {
      if (x.length === 0) {
        let timestring = Math.round(new Date().getTime() / 1000)
        let datestring = new Date().toISOString()
        mysqlQuery({
          sql: 'INSERT INTO `user` (username,password,identifier,datestring,timestring)VALUE(?,?,?,?,?)',
          timeout: 40000, // 40s
          values: [payload.username, payload.password, payload.identifier, datestring, timestring]
        }, (x) => {
          if (x.affectedRows === 1) {
            stats(payload, callback, 'register')
          }
        })
      } else {
        console.log('user exists')
        let data = {
          meta: {
            code: 400,
          }
        }
        callbackCheck(callback, data)
      }
    })
  }
}

const authenticate = (payload, callback) => {
  mysqlQuery({
    sql: 'SELECT * FROM `user` LEFT JOIN stats ON user.identifier = stats.identifier WHERE `token` = ?',
    timeout: 40000, // 40s
    values: [payload.token]
  }, (x, f) => {

    if (x.length === 1) {
      payload.username = x[0].username
      payload.identifier = x[0].identifier
      payload.decorator = x[0].decorator
      payload.mute = x[0].mute
      payload.chat = x[0].chat
      payload.vlog = x[0].vlog
      //Giveaway Data
      payload.instagram = x[0].instagram
      payload.twitter = x[0].twitter
      payload.facebook = x[0].facebook
      payload.email = x[0].email
      payload.country = x[0].country
      console.log(x[0].username + ' authenticated')
      emoji(payload, callback, 'authenticate')
    }
    if (x.length === 0) {
      callbackCheck(callback, 400)
    }

  })
}

const settings = (payload, callback) => {
  function change() {
    mysqlQuery({
      sql: 'UPDATE `user` SET `username` = ? WHERE `identifier` = ?',
      timeout: 40000, // 40s
      values: [payload.username, payload.identifier]
    }, (x, f) => {
      if (x.affectedRows === 1) {
        let data = {
          meta: {
            code: 200
          },
          data: {
            username: payload.username
          }
        }
        callbackCheck(callback, data)
      }
    })
  }

  //Change Username
  if (payload.username.length !== 0) {
    mysqlQuery({
      sql: 'SELECT * FROM `user` WHERE `username` = ?',
      timeout: 40000, // 40s
      values: [payload.username]
    }, (x, f) => {
      if (x.length === 1) {
        callbackCheck(callback, 400)
      }
      if (x.length === 0) {
        change()
      }
    })
  }
}

const login = (payload, callback) => {
  payload.token = payload.username + new Date().getTime()
  payload.token = bcrypt.hashSync(payload.token, bcrypt.genSaltSync(8), null)

  if (payload.username.length !== 0) {
    mysqlQuery({
      sql: 'SELECT * FROM `user` LEFT JOIN stats ON user.identifier = stats.identifier WHERE `username` = ?',
      timeout: 40000, // 40s
      values: [payload.username]
    }, (x) => {
      let validate = false
      if (x.length !== 0) {
        validate = bcrypt.compareSync(payload.password, x[0].password);
      }

      if (validate === true) {
        payload.identifier = x[0].identifier
        payload.decorator = x[0].decorator
        payload.mute = x[0].mute
        payload.chat = x[0].chat
        payload.vlog = x[0].vlog
        //Giveaway Data
        payload.instagram = x[0].instagram
        payload.twitter = x[0].twitter
        payload.facebook = x[0].facebook
        payload.email = x[0].email
        payload.country = x[0].country
        let datestring = new Date().toISOString()
        mysqlQuery({
          sql: 'UPDATE `user` SET `token` = ?, `lastactive` = ? WHERE `identifier`=?',
          timeout: 40000, // 40s
          values: [payload.token, datestring, payload.identifier]
        }, (x) => {
          if (x.affectedRows === 1) {
            console.log(payload.username + ' logged in')
            emoji(payload, callback, 'authenticate')
          }
        })
      } else {
        callbackCheck(callback, 400)
      }
    })
  }
}

module.exports = {
  login: login,
  authenticate: authenticate,
  register: register,
  emoji: emoji,
  stats: stats,
  chat: chat,
  settings: settings,
  giveaway: giveaway,
  moderating: moderating
}