//Default
$(()=>{ $('.content--video').hover(
	()=>{ $('.content--toolbar').removeClass('hide') },
	()=>{ $('.content--toolbar').addClass('hide') }
) })

eventKeydown(true)
$('.l--form input').focus(()=>{eventKeydown(false)})
$('.l--form input').focusout(()=>{eventKeydown(true)})
$("body").tooltip({
	selector: '[data-toggle="tooltip"]'
})

if ($(window).width() < 480 || $(window).height() < 480) {
	$('.js-play-btn').removeClass('hide')
	$('.js-logout').parent().addClass('hide')
	$('.toolbar-desktop').addClass('hide')
	$('.toolbar-mobile').removeClass('hide')	
}

//LA Time
setInterval(()=>{
	let utcTime = new Date
	let laOffset = -7*60*60000
	let userOffset = utcTime.getTimezoneOffset()*60000
	let laTime = new Date(utcTime.getTime()+laOffset+userOffset)
	let hours = laTime.getHours()
	let minutes = laTime.getMinutes()
	let mid = 'AM'
	if (minutes < 10)
	{
		minutes = ('0'+minutes.toString())
	}
	if(hours === 0){
		hours = 12
	}
	else if(hours > 12)
	{
		hours = hours%12
		mid = 'PM'
	}
	$('.js-la-time').text(hours+':'+minutes+' '+mid)
}, 1000)

//Factory
const clickTrigger = (trigger, target) => {
	multiTouch(trigger,()=>{
		$('.l--view').addClass('hide')
		$(target).parent().removeClass('hide')
	})
}

const clickListen = (trigger,callback) => {
	multiTouch(trigger,callback)
}

function toggleView(payload){
	let element = payload.element
	let option = payload.option
	if(option === undefined){
		if($(element).hasClass('hide')){
			$(element).removeClass('hide')
		}
		else{
			$(element).addClass('hide')
		}
	}
	if(option === 'hide'){
		$(element).addClass('hide')
	}
	if(option === 'view'){
		$(element).removeClass('hide')
	}
}

function toggleFullScreen(){
	if ((document.fullScreenElement && document.fullScreenElement !== null) ||
	(!document.mozFullScreen && !document.webkitIsFullScreen)) {
		if (document.documentElement.requestFullScreen) {
			document.documentElement.requestFullScreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullScreen) {
			document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}

		$('.content').addClass('col-md-12')
		$('.content').removeClass('col-md-9')
		$('.chat').addClass('hide')

	} else {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}

		$('.content').removeClass('col-md-12')
		$('.content').addClass('col-md-9')
		$('.chat').removeClass('hide')
	}
}

function eventKeydown(state){
	if(state === true){
		$(document).keydown(()=>{$('.js-m').focus()})
	}
	if(state === false){
		$(document).off('keydown')
	}
}

//Execute Factory
clickTrigger('.js-btn-register','.js-register');
clickTrigger('.js-btn-login','.js-login');
clickListen('.js-fullscreen',toggleFullScreen)
clickListen('.js-sound',toggleSound)
clickListen('.js-emoji-btn',()=>{
	toggleView({element:'.js-emoji-panel'})
})
clickListen('.js-play-btn',()=>{
	$('.content--play-btn').addClass('hide')
	resumeVideo()
})
clickListen('.js-show-user',()=>{
	if($('.chat--online').hasClass('hide')){
		$('.chat--menu').addClass('hide')
		$('.chat--online').removeClass('hide')
	}
	else{
		$('.chat--menu').addClass('hide')
		$('.chat--messages').removeClass('hide')
	}
})
clickListen('.js-profile',()=>{
	if($('.chat--profile').hasClass('hide')){
		$('.chat--menu').addClass('hide')
		$('.js-chat').addClass('hide')
		$('.chat--profile').removeClass('hide')
	}
	else{
		$('.chat--menu').addClass('hide')
		$('.js-chat').removeClass('hide')
		$('.chat--messages').removeClass('hide')
	}
})
clickListen('.js-giveaway', () => {
	if ($('.chat--giveaway').hasClass('hide')) {
		$('.chat--menu').addClass('hide')
		$('.js-chat').addClass('hide')
		$('.chat--giveaway').removeClass('hide')
	}
	else {
		$('.chat--menu').addClass('hide')
		$('.js-chat').removeClass('hide')
		$('.chat--messages').removeClass('hide')
	}
})
clickListen('.js-scoreboard', () => {
	if ($('.chat--scoreboard').hasClass('hide')) {
		$('.chat--menu').addClass('hide')
		$('.js-chat').addClass('hide')
		$('.chat--scoreboard').removeClass('hide')
	}
	else {
		$('.chat--menu').addClass('hide')
		$('.js-chat').removeClass('hide')
		$('.chat--messages').removeClass('hide')
	}
})
clickListen('.js-chat-toggle', ()=>{
	if($('.content').hasClass('col-md-12')){
		$('.content').removeClass('col-md-12')
		$('.content').addClass('col-md-9')
		$('.chat').removeClass('hide')
	}
	else{
		$('.content').addClass('col-md-12')
		$('.content').removeClass('col-md-9')
		$('.chat').addClass('hide')
	}
})

function chatBottom(){
	let cb = document.getElementsByClassName("js-messages")[0];
	if(cb.scrollHeight - cb.scrollTop - cb.clientHeight <= 100){
		cb.scrollTop = cb.scrollHeight;
	}
}
