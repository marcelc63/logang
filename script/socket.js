$(function () {
	let packet = {};
	let store = {
		isLoggedIn: false,
		emoji: [],
		notif: 0,
		initiated: false,		
		streamState: 'streaming',
		user: {
			username: ''
		}
	}

	//Account state
	const accountState = () => {
		if (store.isLoggedIn === true) {
			$('.v-hide').addClass('hide')
			$('.v-loggedIn').removeClass('hide')
		}
		if (store.isLoggedIn === false) {
			$('.v-hide').addClass('hide')
			$('.v-loggedOut').removeClass('hide')
		}
	}

	//Emoji
	const emojiInitiate = () => {
		multiTouch('.js-emoji.emoji--click', (e) => {
			let em = $(e.target).data("emoji")
			let m = $('.js-m').val()
			m = m + ' :' + em + ' '
			$('.js-m').val(m)
			$('.js-m').focus()
		})
	}

	const emoji = (emoji, emojiStore) => {
		interact('.js-emoji.emoji--click').off('tap')
		$('.js-emoji-store').empty()		
		emojiStore.forEach((x) => {
			let val = x.val
			let reason = x.reason
			let src = 'emoji/' + val + '.png'
			let img = $('<img>').attr({
				'src': src,
				'data-toggle': 'tooltip',
				'data-emoji': val
			}).addClass('js-emoji emoji--thumb')
			if (emoji.filter(x => x === val).length === 1) {
				img.addClass('emoji--click').attr({
					'title': reason
				})
			}
			$('.js-emoji-store').append(img)
		})
		emojiInitiate()			
	}

	//Functions

	const getQueryVariable = (variable) => {
		let query = window.location.search.substring(1);
		let vars = query.split("&");
		for (let i = 0; i < vars.length; i++) {
			let pair = vars[i].split("=");
			if (pair[0] === variable) {
				return pair[1];
			}
		}
		return (false);
	}

	const setCookie = (name, value, days = 7, path = '/') => {
		const expires = new Date(Date.now() + days * 864e5).toGMTString()
		document.cookie = name + '=' + encodeURIComponent(value) + '; expires=' + expires + '; path=' + path
	}

	const getCookie = (name) => {
		return document.cookie.split('; ').reduce((r, v) => {
			const parts = v.split('=')
			return parts[0] === name ? decodeURIComponent(parts[1]) : r
		}, '')
	}

	const deleteCookie = (name, path) => {
		setCookie(name, '', -1, path)
	}

	//Listeners

	let socket = io().connect();
	socket.on('connect', function () {
		let token = getCookie('token');
		if (token !== 'undefined') {
			socket.emit('authenticate', token);
		}
		console.log('connected')
		socket.emit('connected');
	});

	socket.on('initiate', function (packet) {
		console.log('initiated')		
		syncVideo(packet.videoStore, 'initiate')
	})

	socket.on('premiere', function (packet) {
		premiereVideo(packet.premiereStart, packet.videoStore, packet.option)
		//syncVideo(packet.videoStore, packet.option)
		setRepetition((i) => {
			let li = $('<li>')
			let ntf = 'l--notification'
			let sec = 4 - i
			let msg = ''
			if (sec !== 0) {
				msg = '<span>Premiere in ' + sec + '</span>'
			} else {
				msg = '<span>Premiere Now!</span>'
			}
			li.addClass(ntf).html(msg)
			$('.js-messages').append(li)
		}, 1000, 4)
		$('.js-premiere-offline').addClass('hide')
		$('.js-premiere-online').removeClass('hide')
	})

	socket.on('authenticate', function (packet) {			
		setCookie('token', packet.token)
		store.isLoggedIn = packet.isLoggedIn
		store.emoji = packet.emoji
		store.user.username = packet.username
		emoji(store.emoji, packet.emojiStore)
		accountState()
		$('.js-m').focus();
		$('.js-points-chat').text(packet.chat)
		$('.js-points-vlog').text(packet.vlog)
		$('.js-points-total').text(packet.total)
		//Giveaway
		$('.js-val-instagram').text(packet.instagram)
		$('.js-val-twitter').text(packet.twitter)
		$('.js-val-facebook').text(packet.facebook)
		$('.js-val-email').text(packet.email)
		$('.js-val-country').text(packet.country)
	});

	socket.on('scoreboard', function (packet) {
		packet.forEach(x=>{
			let li = $('<li>')
			let text = x.username + ' | ' + x.points
			let spr = $('<span>').text(text)
			if (x.decorator !== undefined) {
				spr.addClass('l--' + x.decorator)
			}
			li.append(spr)
			$('.js-scores').append(li);
		})		
	});

	socket.on('emoji', function (packet) {
		store.emoji = packet.emoji
		emoji(store.emoji, packet.emojiStore)
	});

	socket.on('error', function (packet) {
		if (packet.purpose === 'login') {
			$('.js-error-login').removeClass('hide');
		}
	});

	socket.on('refreshState', function (packet) {
		setCookie('token', packet.token)
		store.isLoggedIn = packet.isLoggedIn
		store.emoji = packet.emoji
		emoji(store.emoji)
		accountState()
		$('.js-m').focus();
	});

	socket.on('logout', function (packet) {
		deleteCookie('token')
		store.isLoggedIn = false
		accountState()
	})

	socket.on('settings', function (packet) {
		if (packet.code === 200) {
			$('.js-settings-notif').removeClass('hide')
			$('.js-settings-notif').text('You\'ve changed your name to ' + packet.username)
		}
		if (packet.code === 400) {
			$('.js-settings-notif').removeClass('hide')
			$('.js-settings-notif').text('Username is taken')
		}
	})

	socket.on('chat', function (packet) {
		let li = $('<li>')

		if (packet.usr !== undefined) {
			let usr = $('<span>').addClass('l--usr').html(packet.usr)
			let spr = $('<span>').text(': ')
			if (packet.decorator !== undefined) {
				usr.addClass('l--' + packet.decorator)
			}
			let msg = $('<span>').addClass('l--msg').html(packet.msg)
			li.append(usr).append(spr).append(msg)

			if (packet.usr === store.user.username) {
				//li.addClass('l--mention')
			}
		}

		if (packet.ntf !== undefined) {
			li.addClass(packet.ntf).html(packet.msg)
		}

		$('.js-messages').append(li);
		$('.js-m').focus();
		chatBottom()

		if (!vis()) {
			store.notif = store.notif + 1
			document.title = '(' + store.notif + ') Logangster - Logan Paul Vlogs Live Streaming';
		}
	});

	socket.on('connectChat', function (packet) {
		if (store.initiated === false) {
			store.initiated = true
			packet.chatHistory.reverse().forEach((x) => {
				let li = $('<li>')
				let usr = $('<span>').addClass('l--usr').html(x.username)
				if (x.decorator !== undefined) {
					usr.addClass('l--' + x.decorator)
				}
				let spr = $('<span>').text(': ')
				let msg = $('<span>').addClass('l--msg').html(x.message)
				li.append(usr).append(spr).append(msg)
				$('.js-messages').append(li);
				$('.js-m').focus();
				chatBottom()
			})
		}
	})

	socket.on('connectMeta', function (packet) {
		if (packet.userOnline !== undefined) {
			$('.js-user-online').empty()
			packet.userOnline.forEach((x) => {
				let li = $('<li>')
				let text = x.username + ' | ' + x.points
				let spr = $('<span>').text(text)
				if (x.decorator !== undefined) {
					spr.addClass('l--' + x.decorator)
				}
				li.append(spr)
				$('.js-user-online').append(li);
			})
		}
		if (packet.countConnect !== undefined) {
			$('.js-count').text(packet.countConnect);
		}
	});

	socket.on('startVideo', function (packet) {
		loadVideo(packet, 0)
	});


	//Triggers

	$('.js-chat').submit(function () {
		packet.msg = $('.js-m').val();
		if (store.isLoggedIn === true && packet.msg.length !== 0) {			
			if(packet.msg.length >= 2){
				socket.emit('chat', packet);
			}
			if(packet.msg.length < 2){
				socket.emit('chat-ntf')
			}
		}
		toggleView({
			element: '.js-emoji-panel',
			option: 'hide'
		})
		$('.js-m').val('');
		return false;
	});

	$('.js-login').submit(function () {
		packet.username = $('.js-login .js-username').val();
		packet.password = $('.js-login .js-password').val();
		socket.emit('login', packet);
		return false;
	});

	$('.js-register').submit(function () {
		packet.username = $('.js-register .js-username').val();
		packet.password = $('.js-register .js-password').val();
		socket.emit('register', packet);
		return false;
	});

	$('.js-settings').click(function () {
		packet.username = $('.js-settings .js-username').val();
		socket.emit('settings', packet);
		return false;
	})

	$('.js-form-giveaway').click(function () {
		packet.instagram = $('.js-form-giveaway .js-val-instagram').val();
		packet.twitter = $('.js-form-giveaway .js-val-twitter').val();
		packet.facebook = $('.js-form-giveaway .js-val-facebook').val();
		packet.email = $('.js-form-giveaway .js-val-email').val();
		packet.country = $('.js-form-giveaway .js-val-country').val();
		socket.emit('giveaway', packet);
		return false;
	})

	$('.js-logout').click(function () {
		socket.emit('logout')
		interact('.js-emoji.emoji--click').off('tap')
	})

	let buffer = 0
	$(document).on('vlogEnded', function () {
		if (store.isLoggedIn === true) {
			if (buffer === 0) {
				buffer = 1
				console.log('Vlog Ended!')
				socket.emit('vlog');
			}
			setTimeout(function () {
				buffer = 0
			}, 3000)
		}
	})

	//Default

	accountState()
	vis(() => {
		if (vis()) {
			store.notif = 0
		}
		document.title = 'Logangster - Logan Paul Vlogs Live Streaming'
	});

});